// Fill out your copyright notice in the Description page of Project Settings.


#include "SlimeController.h"
#include "SlimeRoller.h"

ASlimeController::ASlimeController()
{
	CameraSpeed = 3.f;
}

void ASlimeController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("JumpButton", IE_Pressed, this, &ASlimeController::JumpPressed);
	InputComponent->BindAction("JumpButton", IE_Released, this, &ASlimeController::JumpReleased);
	InputComponent->BindAction("TestButton", IE_Pressed, this, &ASlimeController::Shrink);

	InputComponent->BindAxis("CameraPitchAxis", this, &ASlimeController::CamPitch);
	InputComponent->BindAxis("CameraYawAxis", this, &ASlimeController::CamYaw);
	InputComponent->BindAxis("MoveForwardAxis", this, &ASlimeController::MoveForward);
	InputComponent->BindAxis("MoveRightAxis", this, &ASlimeController::MoveRight);
}

void ASlimeController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	Slime = Cast<ASlimeRoller>(InPawn);
}

//=============================================================
// CAMERA
//=============================================================

void ASlimeController::CamPitch(float AxisValue)
{
	//AddPitchInput(AxisValue);
	FRotator CamAngle = ControlRotation;
	CamAngle.Pitch = FMath::Clamp(CamAngle.Pitch + AxisValue * CameraSpeed, -85.f, 40.f);
	ControlRotation = CamAngle;
}

void ASlimeController::CamYaw(float AxisValue)
{
	ControlRotation.Yaw += AxisValue * CameraSpeed;
}

//=============================================================
// MOVEMENT
//=============================================================

void ASlimeController::MoveForward(float AxisValue)
{
	FRotator Rot = FRotator(0.f, ControlRotation.Yaw, 0.f);
	FVector Forward = Rot.RotateVector(FVector::ForwardVector);

	if (Slime)
	{

		UE_LOG(LogTemp, Warning, TEXT("Reached here %f"), AxisValue);
		Slime->AddMovementInput(Forward, AxisValue);
	}
}

void ASlimeController::MoveRight(float AxisValue)
{
	FRotator Rot = FRotator(0.f, ControlRotation.Yaw, 0.f);
	FVector Right = Rot.RotateVector(FVector::RightVector);

	if (Slime)
	{
		Slime->AddMovementInput(Right, AxisValue);
	}
}

void ASlimeController::JumpPressed()
{
	if (Slime)
	{
		Slime->Jump();
	}
}

void ASlimeController::JumpReleased()
{
	if (Slime)
	{
		Slime->StopJumping();
	}
}

void ASlimeController::Shrink()
{
	if (Slime)
	{
		Slime->Resize(EResizeMethod::RM_Shrink);
	}
}
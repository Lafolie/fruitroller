// Fill out your copyright notice in the Description page of Project Settings.


#include "SlimeRoller.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CollisionChannels.h"

// Sets default values
ASlimeRoller::ASlimeRoller()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set some defaults
	InitialRadius = 60.f;
	ResizeAmount = 0.1f;
	Resize(EResizeMethod::RM_Recalc);
	PitchRotationSpeed = -1.f;

	JumpMaxHoldTime = 16.f / 60.f;

	// Setup components
	// Camera
	bUseControllerRotationYaw = false;

	// CharacterMovement
	UCharacterMovementComponent* CharMove = GetCharacterMovement();
	if (CharMove)
	{
		CharMove->bOrientRotationToMovement = true;
		CharMove->RotationRate = FRotator(0.f, 720.f, 0.f);

		CharMove->AirControl = 0.8f;
	}

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 500.f;
	SpringArm->bUsePawnControlRotation = true;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);

	// Mesh
	SphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SphereMesh"));
	SphereMesh->SetupAttachment(RootComponent);
	SphereMesh->bAbsoluteScale = true; // Use absolute scale since we are switching meshes
	SphereMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// Configure capsule
	UCapsuleComponent* Capsule = GetCapsuleComponent();
	if (Capsule)
	{
		Capsule->SetCollisionResponseToChannel(COLLISION_PICKUP, ECR_Overlap);
		Capsule->InitCapsuleSize(InitialRadius, InitialRadius);
	}

	OnCharacterMovementUpdated.AddDynamic(this, &ASlimeRoller::MovementUpdateDelegate);
}

#if WITH_EDITOR
void ASlimeRoller::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	FName PropName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if (PropName == GET_MEMBER_NAME_CHECKED(ASlimeRoller, InitialRadius))
	{
		UCapsuleComponent* Capsule = GetCapsuleComponent();
		if (Capsule)
		{
			Capsule->SetCapsuleSize(InitialRadius, InitialRadius, false);
			Resize(EResizeMethod::RM_Recalc);
		}
	}

	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif

// Called when the game starts or when spawned
void ASlimeRoller::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASlimeRoller::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called when CharacterMovement is updated
void ASlimeRoller::MovementUpdateDelegate(float DeltaSeconds, FVector OldLocation, FVector OldVelocity)
{
	float Pitch = GetVelocity().Size2D() * PitchRotationSpeed * DeltaSeconds;
	SphereMesh->AddLocalRotation(FRotator(Pitch, 0.f, 0.f));
	//SphereMesh->AddRelativeRotation(FRotator(Pitch, 0.f, 0.f));

}

void ASlimeRoller::Resize(EResizeMethod Method)
{
	FVector Delta = FVector::OneVector * ResizeAmount * (int8)Method;
	FVector NewScale = GetActorScale3D() + Delta;
	SetActorScale3D(NewScale);

	// Scale the spheremesh separately due to distance fields not playing with scale nicely
	if (!SphereMesh)
	{
		return;
	}

	int32 NewIndex = FindBand(NewScale.X);
	if (NewIndex > -1)
	{
		CurrentBandIndex = NewIndex;
		FMeshScaleBand& Band = ScaleBands[CurrentBandIndex];
		
		SphereMesh->SetStaticMesh(Band.Mesh);

		// Set correct scale relative to the new band
		FVector AdjustedScale = NewScale / (FVector::OneVector * Band.ScaleLowerThreshold);
		SphereMesh->SetWorldScale3D(AdjustedScale);
	}

}

int32 ASlimeRoller::FindBand(float Scale)
{
	int32 Result = -1;
	for (int32 n = 0; n < ScaleBands.Num(); ++n)
	{
		FMeshScaleBand& Band = ScaleBands[n];
		if (Band.ScaleLowerThreshold <= Scale)
		{
			Result = n;
		}
		else
		{
			break;
		}
	}

	return Result;
}

//// Called to bind functionality to input
//void ASlimeRoller::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
//{
//	Super::SetupPlayerInputComponent(PlayerInputComponent);
//
//}


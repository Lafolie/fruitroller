// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FruitRollerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FRUITROLLER_API AFruitRollerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

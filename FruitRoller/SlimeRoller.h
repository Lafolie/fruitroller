// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "SlimeRoller.generated.h"

enum class EResizeMethod : int8
{
	RM_Shrink = -1, // Shrink the mesh
	RM_Recalc = 0, // Recalculate band
	RM_Grow = 1 // Enlarge the mesh
};

USTRUCT()
struct FMeshScaleBand
{
	GENERATED_BODY()

	// Mesh will be swapped when uniform scale drops below this value
	UPROPERTY(EditAnywhere)
	float ScaleLowerThreshold;

	// Mesh to use when this band is active
	UPROPERTY(EditAnywhere)
	UStaticMesh* Mesh;

	/*FMeshScaleBand()
		: ScaleLowerThreshold(0.f),
		ScaleUpperThreshold(0.f)
	{
		Mesh = nullptr;
	}*/
};

UCLASS()
class FRUITROLLER_API ASlimeRoller : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASlimeRoller();

	// Components
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* SphereMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UArrowComponent* RotArrow;


public:
	// Properties

	// Initial radius for collision capsule
	UPROPERTY(EditDefaultsOnly)
	float InitialRadius;

	// Amount to adjust scale by (applied by linear addition/subtraction)
	UPROPERTY(EditAnywhere)
	float ResizeAmount;

	UPROPERTY(EditAnywhere)
	float PitchRotationSpeed;

	// Meshes used for different scales. Required because of destance field artefacts.
	// Take care when configuring to ensure there is no overlap between bands.
	// Array should be sorted from smallest (index 0) to biggest (index N)
	UPROPERTY(EditAnywhere)
	TArray<FMeshScaleBand> ScaleBands;

	UPROPERTY(EditDefaultsOnly)
	int32 InitialBandIndex;

	UPROPERTY(VisibleAnywhere)
	int32 CurrentBandIndex;

#if WITH_EDITOR
public:
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

private:

	

	// Find the scale band we should use for the given scale
	int32 FindBand(float Scale);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Delegate for MovementUpdated, used to rotate sphere mesh
	UFUNCTION()
	void MovementUpdateDelegate(float DeltaSeconds, FVector OldLocation, FVector OldVelocity);

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

		// Resize the slime according to the @ResizeAmount property.
	// This function may swap the currently used mesh to reduce distance field artefacts
	void Resize(EResizeMethod Method);
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SlimeRoller.h"
#include "SlimeController.generated.h"

/**
 * 
 */
UCLASS()
class FRUITROLLER_API ASlimeController : public APlayerController
{
	GENERATED_BODY()

private:
	UPROPERTY(VisibleInstanceOnly)
	ASlimeRoller* Slime;
	
public:
	ASlimeController();

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	// Called when a pawn is posessed
	virtual void OnPossess(APawn* InPawn) override;

	// Control vars
	UPROPERTY(EditAnywhere, Category = "Camera Controls")
	float CameraSpeed;

	// Input delegates
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void CamPitch(float AxisValue);
	void CamYaw(float AxisValue);

	void JumpPressed();
	void JumpReleased();
	void Shrink();
};
